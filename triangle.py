#!/usr/bin/env python3

class Triangle:
    def __init__(self, side1, side2, side3):
        self.a = side1
        self.b = side2
        self.c = side3

    def perimeter(self):
        perimeter = self.a + self.b + self.c
        return perimeter

    def area(self):
        s = self.perimeter() / 2
        area = (s*(s-self.a)*(s-self.b)*(s-self.c))**0.5
        return area

tri1 = Triangle(3,4,5)

print(tri1.perimeter())
print(tri1.area())